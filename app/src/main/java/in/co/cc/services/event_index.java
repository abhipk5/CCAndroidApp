package in.co.cc.services;

import java.util.ArrayList;

import in.co.cc.model.Page;
import in.co.cc.model.User;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;


/**
 * Created by SONY on 3/17/2016.
 */
public interface event_index {

    @GET(value = "event_index/getPage")
    //here is the other url part.best way is to start using /
    Call<Page> getFeed( @Query("id")String id);     //string user is for passing values from edittext for eg: user=basil2style,google
    //response is the response from the server which is now in the POJO


    @GET("event_index/getPopularEvents")      //here is the other url part.best way is to start using /
    Call<ArrayList<Page>> getPopularEvents();     //string user is for passing values from edittext for eg: user=basil2style,google
    //response is the response from the server which is now in the POJO

    @FormUrlEncoded
    @POST("login/login")
    Call<User> login(@Field("username") String username, @Field("password") String password);

    @GET("login/logout")
    Call<Void> logout();
    @GET("sessioncheck/user")
    Call<User> user();
}
