package in.co.cc.androidServices;

import android.content.Intent;

import java.net.CookieManager;
import java.net.CookieStore;
import java.net.HttpCookie;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import in.co.cc.collegecorner.LoggedIn;
import in.co.cc.model.User;
import in.co.cc.services.event_index;
import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;


/**
 * Created by SONY on 4/8/2016.
 */
public class logInServices {
    private String sessionID;
    String url = "http://collegecorner.co.in/collegesvc/";

    private User currUser=null;

    public User getCurrUser() {
        return currUser;
    }

    public void setCurrUser(User currUser) {
        this.currUser = currUser;
    }

    public String getSessionID() {
        return sessionID;
    }

    public void setSessionID(String sessionID) {
        this.sessionID = sessionID;
    }

    public logInServices() {
    }

    public boolean isLoggedIn(){
        return true;
    }


    public static  OkHttpClient getCookieClient(String phpsessid){
        CookieManager cookieManager= new CookieManager();
            if(phpsessid==null){
                return null;
            }

        HttpCookie cookie = new HttpCookie("PHPSESSID",phpsessid.substring(10,phpsessid.length()-8));
        cookie.setPath("/");
        cookie.setDomain("collegecorner.co.in");
        CookieStore cookieStore=cookieManager.getCookieStore();
        try {
            cookieStore.add(new URI("collegecorner.co.in"),cookie);
        } catch (URISyntaxException e) {
            System.out.print(e);
        }

        OkHttpClient httpClient = new OkHttpClient.Builder().cookieJar(new JavaNetCookieJar(cookieManager)).build();
        return httpClient;
    }

    public static final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            System.out.println(e);
        }
        return "";
    }


}
