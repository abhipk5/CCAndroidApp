package in.co.cc.androidServices;

import android.content.Context;
import android.content.SharedPreferences;

import java.io.IOException;
import java.net.CookieStore;
import java.net.HttpCookie;
import java.util.HashSet;


import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by SONY on 4/10/2016.
 */
public class AddCookiesInterceptor implements Interceptor {
    private CookieStore cookieStore;

    public AddCookiesInterceptor(CookieStore cookieStore) {
        this.cookieStore = cookieStore;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        // Request customization: add request headers
        Request request = null;
        // request = chain.request().newBuilder().header("Set-Cookie", "PHPSESSID=5a987fef12ac98a1679a1718fbc69ca9; path=/").build();

        //if (phpsessid != null) {
         //   request = chain.request().newBuilder().header("Set-Cookie", phpsessid).build();
       // } else {
           request = chain.request().newBuilder().header("PHPSESSID", "phpsessid").build();
       // }
        System.out.print(request + request.headers().toString());
        return chain.proceed(request);
    }
}
