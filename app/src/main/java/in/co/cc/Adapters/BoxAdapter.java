package in.co.cc.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import in.co.cc.collegecorner.R;
import in.co.cc.model.Box;
import in.co.cc.model.Page;
import in.co.cc.model.Point;

/**
 * Created by SONY on 3/20/2016.
 */
public class BoxAdapter extends BaseAdapter{
    private final Context context;
    private ArrayList<Box> boxes;
    private static LayoutInflater inflater=null;

    public BoxAdapter(Context context, ArrayList<Box> boxes) {
        this.context = context;
        this.boxes = boxes;
    }

    @Override
    public int getCount() {
        return boxes.size();
    }

    @Override
    public Object getItem(int position) {
        if(position >= 0)
            return boxes.get(position);
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.box, parent, false);
        TextView boxTitle=(TextView)rowView.findViewById(R.id.boxTitleView);
        boxTitle.setText(boxes.get(position).getTitle());
        ArrayList<Point> pointList= boxes.get(position).getPoints();

        if(pointList != null){
            ListView pointLV=(ListView)rowView.findViewById(R.id.pointListView);
            pointLV.setAdapter(new PointAdapter(context,pointList));
        }

        return rowView;
    }
}
