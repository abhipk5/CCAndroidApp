package in.co.cc.Adapters;

import android.app.Activity;
import android.app.usage.UsageEvents;
import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import in.co.cc.collegecorner.R;
import in.co.cc.model.Page;

/**
 * Created by XYZ on 2/6/2016.
 */
public class EventAdapter extends BaseAdapter {

    private final Context context;


    private Activity activity;
    private ArrayList<Page> events;
    private static LayoutInflater inflater=null;
    public Resources res;
    Page tempValues=null;
    int i=0;

    public EventAdapter(Context context, ArrayList<Page> events) {
        this.context = context;

       // System.out.println("These are the events " + events.toString()+events.size());
        this.events = events;
    }


    @Override
    public int getCount() {

        if(events.size()<=0)
            return 1;
        return events.size();
    }

    @Override
    public Page getItem(int position) {
        if(position < events.size()){
            events.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.event, parent, false);
        TextView textView = (TextView) rowView.findViewById(R.id.event_name);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.eventImage);

        TextView eventDates = (TextView) rowView.findViewById(R.id.event_dates);



        textView.setText(events.get(position).getName().trim());

        eventDates.setText("Start: " + events.get(position).getStartdate() + " End: " + events.get(position).getEnddate());

        if(events.get(position).getImageURL()== null || events.get(position).getImageURL().isEmpty()){
            imageView.setImageResource(R.mipmap.ic_launcher);
        }else {
            Picasso.with(context)
                    .load(events.get(position).getImageURL())
                    .fit()
                    .centerCrop()
                    .into(imageView);
        }

        return rowView;
    }
}
