package in.co.cc.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import in.co.cc.collegecorner.R;
import in.co.cc.model.Box;
import in.co.cc.model.Point;

/**
 * Created by SONY on 3/20/2016.
 */
public class PointAdapter extends BaseAdapter {
    private final Context context;
    private ArrayList<Point> points;
    private static LayoutInflater inflater=null;

    public PointAdapter(Context context, ArrayList<Point> points) {
        this.context = context;
        this.points = points;
    }

    @Override
    public int getCount() {
        return points.size();
    }

    @Override
    public Object getItem(int position) {
        if(position >= 0)
            return points.get(position);
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.point, parent, false);

        TextView pointTitle=(TextView)rowView.findViewById(R.id.pointTitleView);
        TextView pointDesc=(TextView)rowView.findViewById(R.id.pointDescView);

        pointTitle.setText(points.get(position).getTitle());
        pointDesc.setText(points.get(position).getInfo());

        return rowView;
    }
}
