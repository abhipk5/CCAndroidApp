package in.co.cc.model;

/**
 * Created by SONY on 3/18/2016.
 */
public class PageDetails {
    public Page getEvent() {
        return event;
    }

    public void setEvent(Page event) {
        this.event = event;
    }

    private Page event;

    @Override
    public String toString() {
        return "PageDetails{" +
                "event=" + event +
                '}';
    }

    public PageDetails(Page event) {
        this.event = event;
    }
}
