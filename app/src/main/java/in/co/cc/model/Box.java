package in.co.cc.model;

import java.util.ArrayList;

/**
 * Created by SONY on 3/19/2016.
 */
public class Box {

    private String boxid;
    private String title;
    private String pageid;
    private ArrayList<Point> points;

    public Box(String boxid, String title, ArrayList<Point> points, String pageid) {
        this.boxid = boxid;
        this.title = title;
        this.points = points;
        this.pageid = pageid;
    }

    public String getBoxid() {
        return boxid;
    }

    public void setBoxid(String boxid) {
        this.boxid = boxid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<Point> getPoints() {
        return points;
    }

    public void setPoints(ArrayList<Point> points) {
        this.points = points;
    }

    public String getPageid() {
        return pageid;
    }

    public void setPageid(String pageid) {
        this.pageid = pageid;
    }

    @Override
    public String toString() {
        return "Box{" +
                "boxid='" + boxid + '\'' +
                ", title='" + title + '\'' +
                ", points='" + points + '\'' +
                ", pageid='" + pageid + '\'' +
                '}';
    }
}
