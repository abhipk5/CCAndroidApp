package in.co.cc.model;

/**
 * Created by SONY on 3/19/2016.
 */
public class Point {
    private String pointid;
    private String title;
    private  String info;

    public Point(String pointid, String title, String info) {
        this.pointid = pointid;
        this.title = title;
        this.info = info;
    }

    public String getPointid() {
        return pointid;
    }

    public void setPointid(String pointid) {
        this.pointid = pointid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    @Override
    public String toString() {
        return "Point{" +
                "pointid='" + pointid + '\'' +
                ", title='" + title + '\'' +
                ", info='" + info + '\'' +
                '}';
    }
}
