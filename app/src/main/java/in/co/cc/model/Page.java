package in.co.cc.model;

import java.util.ArrayList;

/**
 * Created by XYZ on 2/6/2016.
 */
public class Page {


    public Page() {
    }

    public String getPageid() {
        return pageid;
    }

    public void setPageid(String pageid) {
        this.pageid = pageid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getStartdate() {
        return startdate;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    public String getEnddate() {
        return enddate;
    }

    public void setEnddate(String enddate) {
        this.enddate = enddate;
    }

    public String getRegstartdate() {
        return regstartdate;
    }

    public void setRegstartdate(String regstartdate) {
        this.regstartdate = regstartdate;
    }

    public String getRegenddate() {
        return regenddate;
    }

    public void setRegenddate(String regenddate) {
        this.regenddate = regenddate;
    }

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    public String getRegstarttime() {
        return regstarttime;
    }

    public void setRegstarttime(String regstarttime) {
        this.regstarttime = regstarttime;
    }

    public String getRegendtime() {
        return regendtime;
    }

    public void setRegendtime(String regendtime) {
        this.regendtime = regendtime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCollegeID() {
        return collegeID;
    }

    public void setCollegeID(String collegeID) {
        this.collegeID = collegeID;
    }

    public String getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(String categoryID) {
        this.categoryID = categoryID;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFestemail() {
        return festemail;
    }

    public void setFestemail(String festemail) {
        this.festemail = festemail;
    }

    public String getEventURL() {
        return eventURL;
    }

    public void setEventURL(String eventURL) {
        this.eventURL = eventURL;
    }

    public String getTeamsize() {
        return teamsize;
    }

    public void setTeamsize(String teamsize) {
        this.teamsize = teamsize;
    }

    public String getBrochure() {
        return brochure;
    }

    public void setBrochure(String brochure) {
        this.brochure = brochure;
    }

    private String pageid;
    private String description;
    private String address;
    private String userID;
    private String rank;
    private String imageURL;
    private String startdate;
    private String enddate;
    private String regstartdate;
    private String regenddate;
    private String starttime;
    private String endtime;
    private String regstarttime;
    private String regendtime;
    private String name;
    private String collegeID;
    private String categoryID;
    private String parentId;
    private String approvalStatus;
    private String type;
    private String festemail;
    private String eventURL;
    private String teamsize;
    private String brochure;
    private College college;
    private ArrayList<Box> boxes;
    private ArrayList<Point> sideBoxes;

    public Page(String pageid, String description, String address, String userID, String rank, String imageURL, String startdate, String enddate, String regstartdate, String regenddate, String starttime, String endtime, String regstarttime, String regendtime, String name, String collegeID, String categoryID, String parentId, String approvalStatus, String type, String festemail, String eventURL, String teamsize, String brochure, College college, ArrayList<Box> boxes, ArrayList<Point> sideBoxes) {
        this.pageid = pageid;
        this.description = description;
        this.address = address;
        this.userID = userID;
        this.rank = rank;
        this.imageURL = imageURL;
        this.startdate = startdate;
        this.enddate = enddate;
        this.regstartdate = regstartdate;
        this.regenddate = regenddate;
        this.starttime = starttime;
        this.endtime = endtime;
        this.regstarttime = regstarttime;
        this.regendtime = regendtime;
        this.name = name;
        this.collegeID = collegeID;
        this.categoryID = categoryID;
        this.parentId = parentId;
        this.approvalStatus = approvalStatus;
        this.type = type;
        this.festemail = festemail;
        this.eventURL = eventURL;
        this.teamsize = teamsize;
        this.brochure = brochure;
        this.college = college;
        this.boxes = boxes;
        this.sideBoxes = sideBoxes;
    }

    @Override
    public String toString() {
        return "Page{" +
                "pageid='" + pageid + '\'' +
                ", description='" + description + '\'' +
                ", address='" + address + '\'' +
                ", userID='" + userID + '\'' +
                ", rank='" + rank + '\'' +
                ", imageURL='" + imageURL + '\'' +
                ", startdate='" + startdate + '\'' +
                ", enddate='" + enddate + '\'' +
                ", regstartdate='" + regstartdate + '\'' +
                ", regenddate='" + regenddate + '\'' +
                ", starttime='" + starttime + '\'' +
                ", endtime='" + endtime + '\'' +
                ", regstarttime='" + regstarttime + '\'' +
                ", regendtime='" + regendtime + '\'' +
                ", name='" + name + '\'' +
                ", collegeID='" + collegeID + '\'' +
                ", categoryID='" + categoryID + '\'' +
                ", parentId='" + parentId + '\'' +
                ", approvalStatus='" + approvalStatus + '\'' +
                ", type='" + type + '\'' +
                ", festemail='" + festemail + '\'' +
                ", eventURL='" + eventURL + '\'' +
                ", teamsize='" + teamsize + '\'' +
                ", brochure='" + brochure + '\'' +
                ", college=" + college +
                ", boxes=" + boxes +
                ", sideBoxes=" + sideBoxes +
                '}';
    }

    public ArrayList<Point> getSideBoxes() {
        return sideBoxes;
    }

    public void setSideBoxes(ArrayList<Point> sideBoxes) {
        this.sideBoxes = sideBoxes;
    }

    public ArrayList<Box> getBoxes() {
        return boxes;
    }

    public void setBoxes(ArrayList<Box> boxes) {
        this.boxes = boxes;
    }

    public College getCollege() {
        return college;
    }

    public void setCollege(College college) {
        this.college = college;
    }


}
