package in.co.cc.model;

/**
 * Created by XYZ on 2/6/2016.
 */
public class User {



    private String firstName;
    private String lastName;
    private Object gender;
    private String emailID;
    private String userID;
    private String creationTime;
    private String updateTime;
    private Object mobile;

    public String getPhpsessid() {
        return phpsessid;
    }

    public void setPhpsessid(String phpsessid) {
        this.phpsessid = phpsessid;
    }

    private String phpsessid;

    public User(String firstName, String lastName, Object gender, String userID, String emailID, String creationTime, String updateTime, Object mobile, String phpsessid, String imageURL, String name, Object birthdateDB, Object type, Integer notificationCnt) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.userID = userID;
        this.emailID = emailID;
        this.creationTime = creationTime;
        this.updateTime = updateTime;
        this.mobile = mobile;
        this.phpsessid = phpsessid;
        this.imageURL = imageURL;
        this.name = name;
        this.birthdateDB = birthdateDB;
        this.type = type;
        this.notificationCnt = notificationCnt;
    }

    @Override
    public String toString() {
        return "User{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", gender=" + gender +
                ", emailID='" + emailID + '\'' +
                ", userID='" + userID + '\'' +
                ", creationTime='" + creationTime + '\'' +
                ", updateTime='" + updateTime + '\'' +
                ", mobile=" + mobile +
                ", phpsessid='" + phpsessid + '\'' +
                ", imageURL='" + imageURL + '\'' +
                ", name='" + name + '\'' +
                ", birthdateDB=" + birthdateDB +
                ", type=" + type +
                ", notificationCnt=" + notificationCnt +
                '}';
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Object getGender() {
        return gender;
    }

    public void setGender(Object gender) {
        this.gender = gender;
    }

    public String getEmailID() {
        return emailID;
    }

    public void setEmailID(String emailID) {
        this.emailID = emailID;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(String creationTime) {
        this.creationTime = creationTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public Object getMobile() {
        return mobile;
    }

    public void setMobile(Object mobile) {
        this.mobile = mobile;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getBirthdateDB() {
        return birthdateDB;
    }

    public void setBirthdateDB(Object birthdateDB) {
        this.birthdateDB = birthdateDB;
    }

    public Object getType() {
        return type;
    }

    public void setType(Object type) {
        this.type = type;
    }

    public Integer getNotificationCnt() {
        return notificationCnt;
    }

    public void setNotificationCnt(Integer notificationCnt) {
        this.notificationCnt = notificationCnt;
    }

    private String imageURL;
    private String name;
    private Object birthdateDB;
    private Object type;
    private Integer notificationCnt;



}
