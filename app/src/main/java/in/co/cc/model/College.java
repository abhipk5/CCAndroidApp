package in.co.cc.model;

/**
 * Created by XYZ on 2/6/2016.
 */
public class College {

    private String collegeID;
    private String collegeName;
    private String collegeAddress;
    private String email;
    private String city;
    private String state;
    private String country;
    private String imageURL;
    private String websiteURL;
    private String collegeInfo;

    public College() {
    }

    public College(String collegeName, String collegeID, String collegeAddress, String email, String city, String state, String country, String imageURL, String websiteURL, String collegeInfo) {
        this.collegeName = collegeName;
        this.collegeID = collegeID;
        this.collegeAddress = collegeAddress;
        this.email = email;
        this.city = city;
        this.state = state;
        this.country = country;
        this.imageURL = imageURL;
        this.websiteURL = websiteURL;
        this.collegeInfo = collegeInfo;
    }

    public String getCollegeID() {
        return collegeID;
    }

    public void setCollegeID(String collegeID) {
        this.collegeID = collegeID;
    }

    public String getCollegeName() {
        return collegeName;
    }

    public void setCollegeName(String collegeName) {
        this.collegeName = collegeName;
    }

    public String getCollegeAddress() {
        return collegeAddress;
    }

    public void setCollegeAddress(String collegeAddress) {
        this.collegeAddress = collegeAddress;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getWebsiteURL() {
        return websiteURL;
    }

    public void setWebsiteURL(String websiteURL) {
        this.websiteURL = websiteURL;
    }

    public String getCollegeInfo() {
        return collegeInfo;
    }

    public void setCollegeInfo(String collegeInfo) {
        this.collegeInfo = collegeInfo;
    }

    @Override
    public String toString() {
        return "College{" +
                "collegeID='" + collegeID + '\'' +
                ", collegeName='" + collegeName + '\'' +
                ", collegeAddress='" + collegeAddress + '\'' +
                ", email='" + email + '\'' +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                ", country='" + country + '\'' +
                ", imageURL='" + imageURL + '\'' +
                ", websiteURL='" + websiteURL + '\'' +
                ", collegeInfo='" + collegeInfo + '\'' +
                '}';
    }
}
