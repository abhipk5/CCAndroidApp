package in.co.cc.collegecorner;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import java.io.IOException;
import java.util.List;

import in.co.cc.androidServices.logInServices;
import in.co.cc.model.User;
import in.co.cc.public_fragments.AboutUs;
import in.co.cc.public_fragments.Help;
import in.co.cc.public_fragments.LandingPage;
import in.co.cc.public_fragments.SignIn;
import in.co.cc.public_fragments.SignUp;
import in.co.cc.public_fragments.StartActivityFragment;
import in.co.cc.services.event_index;
import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;



public class StartActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    String url = "http://collegecorner.co.in/collegesvc/";
    Retrofit adapter = new Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        Fragment fragment = null;
        Class fragmentClass;
        fragmentClass = LandingPage.class;

        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
    }

    public void logIn(String username, String password){

        SharedPreferences sharedpreferences = getSharedPreferences("phpsessid", Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedpreferences.edit();

        final event_index restInterface = adapter.create(event_index.class);
        logInServices s=new logInServices();
        password=s.md5(password);
        final Intent intent = new Intent(this, LoggedIn.class);
        Call<User> call= restInterface.login(username, password);


        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, retrofit2.Response<User> response) {
                if (response.isSuccessful()) {
                    final User user = response.body();


                    Headers h=response.headers();
                    String sessid=h.get("Set-Cookie");
                    System.out.println("Received SESSID " + sessid);
                    if (user != null) {
                        editor.putString("PHPSESSID",sessid);

                        editor.commit();

                        startActivity(intent);
                    }
                }else{
                    System.out.print("Response incorrect");
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                System.out.print("Response failure");

            }
        });


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.start, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        Fragment fragment = null;

        Class fragmentClass;
        switch(item.getItemId()) {
            case R.id.nav_home:
                System.out.println("Setting landing page ");
                fragmentClass = LandingPage.class;
                break;
            case R.id.nav_signin:
                System.out.println("Setting Sign In page ");
                fragmentClass = SignIn.class;
                break;
            case R.id.nav_signup:
                System.out.println("Setting Sign Up page ");
                fragmentClass = SignUp.class;
                break;
            case R.id.about_us:
                fragmentClass = AboutUs.class;
                break;
            case R.id.help:
                fragmentClass = Help.class;
                break;
            default:
                fragmentClass = LandingPage.class;
        }

        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();

        // Highlight the selected item, update the title, and close the drawer
        item.setChecked(true);
        setTitle(item.getTitle());
        //drawer.closeDrawer(GravityCompat.START);




/*        if (id == R.id.nav_camera) {
            // Handle the camera action





        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }
*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
