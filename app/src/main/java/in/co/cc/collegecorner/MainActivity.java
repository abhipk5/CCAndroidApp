package in.co.cc.collegecorner;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.squareup.picasso.Downloader;

import java.io.IOException;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.CookieStore;
import java.net.HttpCookie;
import java.net.URI;
import java.net.URISyntaxException;

import in.co.cc.model.User;
import in.co.cc.services.event_index;
import okhttp3.Interceptor;
import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class MainActivity extends AppCompatActivity {
    String url = "http://collegecorner.co.in/collegesvc/";




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        SharedPreferences prefs = getSharedPreferences("phpsessid", Context.MODE_PRIVATE);
        String phpsessid=null;
        if(prefs != null){
             phpsessid = prefs.getString("PHPSESSID", null);
        }
        beginActivity(phpsessid);
    }

    public void beginActivity(final String phpsessid) {
        final Intent logInIntent = new Intent(this, LoggedIn.class);
        final Intent publicIntent = new Intent(this, StartActivity.class);
        if (phpsessid == null) {
            startActivity(publicIntent);
        } else {
            CookieManager cookieManager = new CookieManager();


            HttpCookie cookie = new HttpCookie("PHPSESSID", phpsessid.substring(10, phpsessid.length() - 8));
            cookie.setPath("/");
            cookie.setDomain("collegecorner.co.in");
            CookieStore cookieStore = cookieManager.getCookieStore();
            try {
                cookieStore.add(new URI("collegecorner.co.in"), cookie);
            } catch (URISyntaxException e) {
                System.out.print(e);
            }

            OkHttpClient httpClient = new OkHttpClient.Builder().cookieJar(new JavaNetCookieJar(cookieManager)).build();

            System.out.print(cookie);

            Retrofit adapter = new Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).client(httpClient).build();
            final event_index restInterface = adapter.create(event_index.class);


            Call<User> call = restInterface.user();
            call.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, retrofit2.Response<User> response) {
                    if (response.isSuccessful()) {
                        User user = response.body();
                        // System.out.print(user);

                        if (user != null) {
                            startActivity(logInIntent);
                        } else {
                            startActivity(publicIntent);
                        }
                    } else {
                        System.out.print("Not successful");
                        startActivity(publicIntent);
                    }
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    System.out.print(t);
                }
            });


        }
    }
}
