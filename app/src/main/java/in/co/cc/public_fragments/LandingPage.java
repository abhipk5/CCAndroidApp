package in.co.cc.public_fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import in.co.cc.Adapters.EventAdapter;
import in.co.cc.collegecorner.R;
import in.co.cc.model.Page;

import in.co.cc.services.event_index;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class LandingPage extends Fragment {

    String url = "http://collegecorner.co.in/collegesvc/";
    //making object of RestAdapter
    Retrofit adapter = new Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build();
    //Creating Rest Services
    event_index restInterface = adapter.create(event_index.class);
    private Context context;
    private ListView lv;
    private ArrayList<Page> popularEventList = new ArrayList<Page>();


    public LandingPage() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static LandingPage newInstance(String param1, String param2) {
        LandingPage fragment = new LandingPage();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        System.out.println("In start activity fragment ");

        context = this.getActivity();


        final View rootView = inflater.inflate(R.layout.fragment_start,
                container, false);
        lv = (ListView) rootView.findViewById(R.id.eventList);

        //Calling method to get whether report
        Call<ArrayList<Page>> call= restInterface.getPopularEvents();
        call.enqueue(new Callback<ArrayList<Page>>() {
            @Override
            public void onResponse(Call<ArrayList<Page>> call, Response<ArrayList<Page>> response) {
                if (response.isSuccessful()) {
                    popularEventList = response.body();
                    System.out.println(" These are pop events " +popularEventList);
                    lv.setAdapter(new EventAdapter(context, popularEventList));
                } else {
                    System.out.println(" this is error response " + response.code() + response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Page>> call, Throwable t) {
                System.out.println(" this is failure response " + t);
            }
        });




        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Page p = popularEventList.get(position);
                System.out.println("Inside Item Click " + p + position);

                Class fragmentClass;
                fragmentClass = PublicPage.class;
                Fragment fragment1=null;
                try {
                    fragment1 = (Fragment) fragmentClass.newInstance();
                    Bundle args= new Bundle();
                    args.putString("pageid", p.getPageid());
                    fragment1.setArguments(args);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                // Insert the fragment by replacing any existing fragment
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.content_frame, fragment1).commit();


            }
        });

        return rootView;
    }


    public void onButtonPressed(Uri uri) {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */

}
