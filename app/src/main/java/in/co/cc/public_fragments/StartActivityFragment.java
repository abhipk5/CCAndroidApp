package in.co.cc.public_fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

import in.co.cc.Adapters.EventAdapter;
import in.co.cc.collegecorner.R;
import in.co.cc.model.Page;
import in.co.cc.services.event_index;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * A placeholder fragment containing a simple view.
 */
public class StartActivityFragment extends Fragment {

    String url = "http://collegecorner.co.in/collegesvc/";

    private Context context;

    private ListView lv;
    private ArrayList<Page> popularEventList = new ArrayList<Page>();

    //making object of RestAdapter
    Retrofit adapter = new Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build();

    //Creating Rest Services
    event_index restInterface = adapter.create(event_index.class);



    public StartActivityFragment() {



        //Calling method to get whether report


    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        System.out.println("In start activity fragment ");

        context = this.getActivity();



        View rootView = inflater.inflate(R.layout.fragment_start,
                container, false);
        lv = (ListView) rootView.findViewById(R.id.eventList);


        Call<ArrayList<Page>> call= restInterface.getPopularEvents();
        call.enqueue(new Callback<ArrayList<Page>>() {
            @Override
            public void onResponse(Call<ArrayList<Page>> call, Response<ArrayList<Page>> response) {
                if (response.isSuccessful()) {
                    popularEventList = response.body();
                    //System.out.println(" These are pop events " + pages.toString());
                    lv.setAdapter(new EventAdapter(context, popularEventList));
                } else {
                    System.out.println(" this is error response " + response.code() + response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Page>> call, Throwable t) {
                System.out.println(" this is error response " + t);
            }
        });


        System.out.println(" calling event adapter ");


        return rootView;
    }


}