package in.co.cc.public_fragments;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import in.co.cc.Adapters.EventAdapter;
import in.co.cc.androidServices.logInServices;
import in.co.cc.collegecorner.LoggedIn;
import in.co.cc.collegecorner.R;
import in.co.cc.collegecorner.StartActivity;
import in.co.cc.model.Page;

import in.co.cc.model.User;
import in.co.cc.services.event_index;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by XYZ on 2/7/2016.
 */
public class SignIn extends Fragment {

    String url = "http://collegecorner.co.in/collegesvc/";
    //making object of RestAdapter
    Retrofit adapter = new Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build();
    //Creating Rest Services

    private Context context;
    private ListView lv;
    private ArrayList<Page> popularEventList = new ArrayList<Page>();


    public SignIn(){

    }


    // TODO: Rename and change types and number of parameters
    public static SignIn newInstance(String param1, String param2) {
        SignIn fragment = new SignIn();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        context = this.getActivity();

        final View rootView = inflater.inflate(R.layout.fragment_signin,
                container, false);

        System.out.println(" calling event adapter ");
        final event_index restInterface = adapter.create(event_index.class);

        Button clickButton = (Button) rootView.findViewById(R.id.button_login);
        clickButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                EditText userText = (EditText) rootView.findViewById(R.id.edittext_email);
                String username = userText.getText().toString();
                EditText passText = (EditText) rootView.findViewById(R.id.edittext_password);
                String password= passText.getText().toString();

                ((StartActivity)getActivity()).logIn(username, password);

            }
        });

        return rootView;
    }



    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {

    }


}
