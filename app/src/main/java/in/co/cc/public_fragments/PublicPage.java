package in.co.cc.public_fragments;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import in.co.cc.Adapters.BoxAdapter;
import in.co.cc.Adapters.PointAdapter;
import in.co.cc.collegecorner.R;
import in.co.cc.model.Box;
import in.co.cc.model.Page;
import in.co.cc.model.Point;
import in.co.cc.services.event_index;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by SONY on 3/20/2016.
 */
public class PublicPage extends Fragment {
   private Page page;
    String url = "http://collegecorner.co.in/collegesvc/";
    //making object of RestAdapter
    Retrofit adapter = new Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build();
    //Creating Rest Services
    event_index restInterface = adapter.create(event_index.class);

    private Context context;

    public PublicPage() {
        // Required empty public constructor
    }



    // TODO: Rename and change types and number of parameters
    public static PublicPage newInstance() {
        PublicPage fragment = new PublicPage();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this.getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        context = this.getActivity();

        String pageid=this.getArguments().getString("pageid");
        System.out.println("Inside Item Click "+ pageid);
        final View pageView= inflater.inflate(R.layout.page,container,false);
        final TextView eventName=(TextView)pageView.findViewById(R.id.event_name);
        final TextView eventDate= (TextView)pageView.findViewById(R.id.event_dates);
        final ImageView imageView = (ImageView) pageView.findViewById(R.id.eventImage);
        final ListView boxLv=(ListView)pageView.findViewById(R.id.boxesView);
        final ListView sideBoxLv =(ListView)pageView.findViewById(R.id.sideBoxesView);
        Call<Page> call= restInterface.getFeed(
                pageid
        );

        call.enqueue(new Callback<Page>() {
            @Override
            public void onResponse(Call<Page> call, Response<Page> response) {
                if (response.isSuccessful()) {
                    page = response.body();
                    System.out.println(" this is EVENT model " + page.toString() + response.toString());
                    eventName.setText(page.getName());
                    eventDate.setText("Start: " + page.getStartdate() + " " + page.getStarttime() + " End: " + page.getEnddate() +
                            " " + page.getEndtime());

                    if (page.getImageURL() == null || page.getImageURL().isEmpty()) {
                        imageView.setImageResource(R.mipmap.ic_launcher);
                    } else {
                        Picasso.with(context)
                                .load(page.getImageURL())
                                .fit()
                                .centerCrop()
                                .into(imageView);
                    }
                    if (page != null && page.getBoxes() != null) {
                        boxLv.setAdapter(new BoxAdapter(context, page.getBoxes()));
                    }

                    ArrayList<Point> displayBoxes = new ArrayList<Point>(3);
                    displayBoxes.add(0, new Point("1", "Description", page.getDescription()));
                    displayBoxes.add(1, new Point("2", "Team Size", page.getTeamsize()));
                    displayBoxes.add(2, new Point("3", "College", page.getCollege().getCollegeName()));


                    if (page != null && page.getSideBoxes() != null) {
                        page.getSideBoxes().addAll(displayBoxes);
                    } else {
                        page.setSideBoxes(displayBoxes);
                    }
                    sideBoxLv.setAdapter(new PointAdapter(context, page.getSideBoxes()));
                } else {
                    System.out.println(" this is error response " + response.code() + response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<Page> call, Throwable t) {
                System.out.println(" this is error response " + t);
            }
        });

        return pageView;
    }
}
